<?php
namespace rd\bd\dynamic\postid;

use Breakdance\DynamicData\StringField;
use Breakdance\DynamicData\StringData;

class RD_BD_DynamicPostIdField extends StringField
{

	public string $rd_bd_post_type;
	public string $rd_bd_label;
	public string $rd_bd_slug;

	public function rd_bd_set( string $slug, string $label, string $post_type ): RD_BD_DynamicPostIdField
	{
		$this->rd_bd_post_type = $post_type;
		$this->rd_bd_label = $label;
		$this->rd_bd_slug = $slug;
		\Breakdance\DynamicData\registerField( $this );
		return $this;
	}

	public function label()
	{
		return $this->rd_bd_label;
	}

	public function category()
	{
		return 'Post';
	}

	public function slug()
	{
		return $this->rd_bd_slug;
	}

	public function returnTypes()
	{
		return [ 'string' ];
	}

	public function controls()
	{
		return [
			\Breakdance\Elements\control( 'rd_bd_post', 'Post', [
				'type' => 'post_chooser',
				'layout' => 'vertical',
				'postChooserOptions' => [
					'multiple' => false,
					'showThumbnails' => false,
					'postType' => $this->rd_bd_post_type
				]
			])
		];
	}

	public function handler($attributes): StringData
	{
		$value = '';
		if ( isset( $attributes['rd_bd_post'] ) ) {
			$post_id = intval( $attributes[ 'rd_bd_post' ] );
			$value = $post_id . '';
		} else if ( is_singular() ) {
			$value = get_the_ID() . '';
		}
		return StringData::fromString( $value );
	}

}

add_action(
	'init',
	function() {
		$post_types = [ 'post', 'page' ];
		foreach ( $post_types as $post_type ) {
			$class = new class extends RD_BD_DynamicPostIdField {};
			$class->rd_bd_set( 'rd_bd_dynamic_post_id_' . $post_type, 'Post ID ('. $post_type .')', $post_type );
		}
	}
);
